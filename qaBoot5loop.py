### FOR LOOPING ### 02 Maret 2022 ##
# ## 1 ##
print('====For Loop====')
print('====Pertama====')
animal = ['cat', 'dog', 'monkey']
for c in animal:
    print(c, len(c))

# ## 2 ##
print('====Kedua====')
bermain = ['bola', 'balon', 'kelereng']
for m in bermain:
    print(m)

# ## 3 ##
print('====Ketiga====')
olahraga = ['push up', 'sit up', 'jogging']
for d in olahraga:
    print(d)
    print(len(d))

# ## 4 ## 
print('====Keempat====')
bakwan = 'bakwan'
for g in bakwan:
    print(g)

# ## 5 ##
print('====Kelima====')
sepatu = ['neki', 'abibas', 'ardila']
laptop = ['snsv', 'bell', 'lenongvo']

beliBarang = [sepatu, laptop, bermain]
print(beliBarang)

# ## 6 ##
print('====Keenam====List Angka====')
listAngka = [1, 3, 5, 7, 9]
for a in listAngka:
    print(f'a adalah {a}')

# ## 7 ## Range
print('====Ketujuh====Range====')
for f in range(1, 13):
    print(f)

# ## 8 ##
print('====Kedelapan====Range====')
for t in range(10,50,10):
    print(t)

# ## 9 ##
print('====Kesembilan====Mencari Angka====')
# mencariAngka = 5
for k in range(0,8):
    if k is 5:
        print('angka ditemukan', k)
    else:
        print('angka tidak ditemukan', k)

### While Loop ###
## 1 ##
print(30*'=')
whileAngka = 1
print('angka pertamanya adalah', whileAngka)
while whileAngka < 10:
    whileAngka += 3
    print(f'angkanya sekarang adalah {whileAngka}')
