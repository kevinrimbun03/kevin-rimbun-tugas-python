##### CONDITIONAL #####
## 1 ## ## SHORT if ##
print('====Conditional SHORT if====')
a = 5
b = 6
print('a less than b') if a < b else print('greater than b')

## 2 ## ## SHORT if ##
c = 9
d = 8
if c > d: print('c greater than d')

## Tipe Data String ##
print('====Tipe Data String====')
dataString = ("ini pakai string")
print(dataString, "tipe datanya adalah", type(dataString))

## Tipe data Integer ##
print('===Tipe Data Integer')
dataInteger = 3
print(dataInteger, 'tipe data nya adalah', type(dataInteger))

## Tipe Data Complex with Arithmetic ##
print('====Tipe Data Complex with Arithmetic====')
## Contohnya ###
a = 5j
b = 10j
c = a + b
print(a, '+', b, '=', c, type(a))

## Tipe data Boolean ##


## Casting Integer ##
print('====Ini adalah Casting Integer====')
dataCastingInteger = 13
print(dataCastingInteger, "tipe datanya dalah", type(dataCastingInteger))

data_float = float(dataCastingInteger)
data_string = str(dataCastingInteger)
data_boolean = bool(dataCastingInteger)

print("data", data_float, "tipenya adalah", type(data_float))
print("data", data_string, "tipenya adalah", type(data_string))
print("data", data_boolean, "tipenya adalah", type(data_boolean))

## Casting STRING ##
print('====Ini adalah Casting String====')
dataCastingString = "13"
print(dataCastingString, "tipe datanya dalah", type(dataCastingString))

data_float = float(dataCastingString)
data_integer = int(dataCastingString)
data_boolean = bool(dataCastingString)

print("data", data_float, "tipenya adalah", type(data_float))
print("data", data_integer, "tipenya adalah", type(data_integer))
print("data", data_boolean, "tipenya adalah", type(data_boolean))

## Casting Float ##
print('====Ini adalah Casting Float====')
dataCastingFloat = 15.5
print(dataCastingFloat, "tipe datanya dalah", type(dataCastingFloat))

data_string = str(dataCastingFloat)
data_integer = int(dataCastingFloat)
data_boolean = bool(dataCastingFloat)

print("data", data_string, "tipenya adalah", type(data_string))
print("data", data_integer, "tipenya adalah", type(data_integer))
print("data", data_boolean, "tipenya adalah", type(data_boolean))

## Input Data ##
print('====Input data String===')
inputString = input('masukkan data String: ')
print('tipe', inputString, 'ini adalah', type(inputString))

## Input Data ##
print('====Input data Integer===')
inputInt = int(input('masukkan data Integer: '))
print('tipe', inputInt, 'ini adalah', type(inputInt))