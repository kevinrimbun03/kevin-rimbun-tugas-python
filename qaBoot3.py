#### Conditions if else ####

print('====Input IF ELSE====')
movie = input('kamu suka menonton movie juga? ')
if movie == "iya":
    print('Wah sama kayak aku dong hehe..')
else:
    print('yahh..')

print('====Deklarasi====')
a = 7
if a == 7:
    print('angkanya memang 7')
else:
    print('angkanya bukan 7')

print('====Conditions ELIF====')
makan = input("mau makan apa? ")
if makan == 'ayam goreng':
    print("oke saya buatkan ayam goreng")
elif makan == 'lontong balap':
    print("oke saya buatkan lontong balap")
else:
    print('tidak ada di menu')
