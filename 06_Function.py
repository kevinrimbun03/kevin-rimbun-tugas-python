### 02 Maret 2022 ###
## FUNCTION ##

#1 # def Function

def function():
    print('ini adalah function')

function() ### calling function on line 6 

print(30*'=') 
#keyword argument 1
def pesanTiket(nama, mauapa):
    print('Haloo Kak', nama, mauapa)

### calling function on line 13 ###
pesanTiket('Kevin', 'Mau pesan tiket film apa?')
pesanTiket('Mau pesan tiket film tokekman', 'Untuk Jam 2 siang')

print(30*'=')

def namaOrang(nama1, nama2, nama3):
    print('nama orang kedua adalah', nama2)

namaOrang(nama1='Han Ji Pyeong', nama2='Nam Do San', nama3='Park Saeroyi')
namaOrang(nama2='Nam Do San', nama1='Han Ji Pyeong', nama3='Park Saeroyi')

print(30*'=')

#2 #Arbitrary Argument
def bebas(*presiden):
    print('Presiden ke 3 Indonesia adalah ', presiden[2])

bebas('Soekarno', 'Soeharto', "B.J. Habibie", 'Abdurrahman Wahid')

print(30*'=')

#3 #default parameter value
def default(daerah='Bandung'):
    print('Saya berasal dari Daerah', daerah)

default('Bali')
default()
default('Semarang')

print(30*'=')

#4 #return value
def returnValue(x):
    return 10 * x

print(returnValue(2))

print(30*'=')

#5 #membuat fungsi dengan input argumen
def slogan():
    mauapa = ('Mau pesan apa?')
    print('Selamat datang di Wekidi', mauapa)

def hargatotalayam(paket):
    slogan()
    harga = 20000
    cs = ('Baik kak akan kami siapkan,')
    hargaTotal = paket*harga
    print('Mau pesan 5 paket ayam')
    print(cs, 'harga', paket, 'paket', 'ayamnya adalah', hargaTotal)

hargatotalayam(5)