### 01 Maret 2022 ###
""""""
print('====KALKULATOR====')

angkaSatu = int(input('Masukkan angka: '))
operator = input('Masukkan operator (+, -, *, /): ')
angkaDua = int(input('Masukkan angka: '))

if operator == '+':
    hasil = angkaSatu + angkaDua
    print("Hasilnya adalah: ", hasil)
elif operator == '-':
    hasil = angkaSatu - angkaDua
    print("Hasilnya adalah: ", hasil)
elif operator == '*':
    hasil = angkaSatu * angkaDua
    print("Hasilnya adalah: ", hasil)
elif operator == '/':
    hasil = angkaSatu / angkaDua
    print("Hasilnya adalah: ", hasil)
else:
    print('Silahkan masukkan Operator atau Angka yang Benar')
# except ValueError:
#     print('Non-numeric data found in the file.')


