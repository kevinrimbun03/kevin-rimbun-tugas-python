#OOP Object Oriented Programming

#1 ### create class name ### 
class contoh1:
    tst = 'test' 

object = contoh1()
print(object.tst)

print(30*('='))

#2 #__init__() magic keyword
class contoh2:
    def __init__(coba1, nama, pekerjaan): #instance
        coba1.name = nama
        coba1.job = pekerjaan
        print('test kelasnya')
    
    def function(a):
        print('Haloo', a.name)

object2 = contoh2('Kevin', 'Programmer')
object3 = contoh2('Raffi Ahmad', 'Selebriti')
object4 = contoh2('Raisa', 'Penyanyi')
object2.function()

print(object2.name, 'pekerjaannya', object2.job)
print(object3.name, 'pekerjaannya', object3.job)
print(object4.name, 'pekerjaannya', object4.job)

print(30*('='))

#3 ##2 #__init__() magic keyword
class contoh3:
    def __init__(coba2, makanan, asalnya):
        coba2.food = makanan
        coba2.negara = asalnya

    def function(b):
        print('Nama makanannya adalah', b.food, 'berasal dari negara', b.negara) 

sushi = contoh3('Sushi', 'Jepang')
pizza = contoh3('Pizza', 'Italia')
tomyum = contoh3('Tom Yum', 'Thailand')

print(sushi.food)
print('Asal negaranya adalah', sushi.negara)
sushi.function()
pizza.function()
tomyum.function()
